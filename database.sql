CREATE SCHEMA IF NOT EXISTS administration;

CREATE TABLE IF NOT EXISTS administration.people
(
  id Serial PRIMARY KEY,
  name VARCHAR(20) NOT NULL,
  surname VARCHAR(20) NOT NULL
);

CREATE SCHEMA IF NOT EXISTS log;

CREATE TABLE IF NOT EXISTS log.logs
(
  id Serial PRIMARY KEY,
  date Timestamp NOT NULL,
  table_name VARCHAR(50) NOT NULL,
  operation VARCHAR(10) NOT NULL
);

CREATE OR REPLACE FUNCTION log.log_trig() RETURNS trigger AS 
$$
    BEGIN
       INSERT INTO log.logs(date, table_name, operation)
			VALUES (now(), TG_TABLE_SCHEMA||'.'||TG_TABLE_NAME, TG_OP);
        RETURN NEW;
    END;
$$ 
LANGUAGE plpgsql;

CREATE TRIGGER people_log
AFTER INSERT OR UPDATE OR DELETE ON administration.people
    FOR EACH ROW EXECUTE FUNCTION log.log_trig();